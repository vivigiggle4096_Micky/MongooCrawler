package com.trytech.mongoocrawler.server.transport.http;

import com.trytech.mongoocrawler.server.transport.http.cookie.HttpCookie;
import com.trytech.mongoocrawler.server.transport.http.cookie.HttpCookiesManager;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * http客户端测试类
 */
public class HttpClientTest {
    /**********************
     * 测试doGet类
     *********************/
    @Test
    public void doGet() throws MalformedURLException {
        HttpClient client = new HttpClient();
        CrawlerHttpRequest request = new CrawlerHttpRequest("sdsaddi103kasd3",new URL("https://www.zhihu.com/"),null ,null);
        WebResult<HttpResponseProtocol> result = client.doGet(request);
        List<HttpCookie> list = HttpCookiesManager.getCookies("sdsaddi103kasd3","/");
        System.out.println(list.get(0).getName());
    }
}
