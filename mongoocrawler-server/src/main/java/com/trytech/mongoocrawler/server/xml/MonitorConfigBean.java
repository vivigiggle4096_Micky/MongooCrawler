package com.trytech.mongoocrawler.server.xml;

import lombok.Data;

/**
 * Created by coliza on 2018/5/24.
 */
@Data
public class MonitorConfigBean {
    private int port;
}
