package com.trytech.mongoocrawler.server;

import com.trytech.mongoocrawler.server.common.exception.DataSourceInitException;
import com.trytech.mongoocrawler.server.xml.XmlConfigBean;
import com.trytech.mongoocrawler.server.xml.XmlDocumentBuilder;
import org.dom4j.DocumentException;
import org.xml.sax.SAXException;

import java.io.IOException;

/**
 * Created by hp on 2017-2-17.
 */
public class CrawlerConfig {
    private static XmlConfigBean configBean;

    public static CrawlerConfig newInstance(String path) throws IOException, DocumentException, SAXException, DataSourceInitException {
        XmlDocumentBuilder xmlDocumentBuilder = XmlDocumentBuilder.newInstance();
        configBean = xmlDocumentBuilder.parse(path);
        CrawlerConfig config = new CrawlerConfig();
        config.setConfigBean(configBean);
        return config;
    }

    public XmlConfigBean getConfigBean() {
        return configBean;
    }

    public void setConfigBean(XmlConfigBean configBean) {
        CrawlerConfig.configBean = configBean;
    }

    public enum CrawlerMode {
        LOCAL_MODE(0, "单机模式"), DISTRIBUTED_MODE(1, "分布式模式");
        private int val;
        private String label;

        CrawlerMode(int val, String label) {
            this.val = val;
            this.label = label;
        }

        public static CrawlerMode paserMode(int value) {
            switch (value) {
                case 1:
                    return DISTRIBUTED_MODE;
                default:
                    return LOCAL_MODE;
            }
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public int getValue() {
            return this.val;
        }

        public boolean equals(CrawlerMode crawlerMode) {
            return this.val == crawlerMode.val;
        }
    }

    public enum URL_STORE_MODE {
        LOCAL("LOCAL"), REDIS("REDIS");
        private String value;

        URL_STORE_MODE(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public boolean equals(URL_STORE_MODE mode) {
            return this.value.equals(mode.getValue());
        }
    }
}
