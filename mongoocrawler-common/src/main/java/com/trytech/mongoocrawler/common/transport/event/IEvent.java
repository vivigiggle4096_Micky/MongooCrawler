package com.trytech.mongoocrawler.common.transport.event;

/**
 * Created by coliza on 2018/9/16.
 */
public interface IEvent {

    IEventSource getSource();

    void setSource(IEventSource source);
}
